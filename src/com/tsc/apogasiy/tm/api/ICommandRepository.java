package com.tsc.apogasiy.tm.api;

import com.tsc.apogasiy.tm.model.Command;

public interface ICommandRepository {

    Command[] getCommands();

}
