package com.tsc.apogasiy.tm;

import com.tsc.apogasiy.tm.api.ICommandRepository;
import com.tsc.apogasiy.tm.constant.ArgumentConst;
import com.tsc.apogasiy.tm.constant.TerminalConst;
import com.tsc.apogasiy.tm.model.Command;
import com.tsc.apogasiy.tm.repository.CommandRepository;
import com.tsc.apogasiy.tm.util.NumberUtil;

import java.util.Scanner;

public class Application {

    private static final ICommandRepository COMMAND_REPOSITORY = new CommandRepository();

    public static void main(final String[] args) {
        showWelcome();
        parseArgs(args);
        process();
    }

    private static void process() {
        final Scanner scanner = new Scanner(System.in);
        String command = "";
        while (!TerminalConst.EXIT.equals(command)) {
            System.out.println("ENTER COMMAND:");
            command = scanner.nextLine();
            parseCommand(command);
            System.out.println();
        }
    }

    public static void showCommands() {
        for (final Command command : COMMAND_REPOSITORY.getCommands()) {
            showCommandValue(command.getName());
        }
    }

    public static void showArguments() {
        for (final Command command : COMMAND_REPOSITORY.getCommands()) {
            showCommandValue(command.getArgument());
        }
    }

    private static void showCommandValue(final String value) {
        if (value == null || value.isEmpty())
            return;
        System.out.println(value);
    }

    private static void showHelp() {
        for (final Command command : COMMAND_REPOSITORY.getCommands()) {
            System.out.println(command);
        }
    }

    private static void showVersion() {
        System.out.println("1.0.0");
    }

    private static void exit() {
        System.exit(0);
    }

    private static void showAbout() {
        System.out.println("Alexey Pogasiy");
        System.out.println("apogasiy@tsconsulting.com");
    }

    private static void showWelcome() {
        System.out.println("** WELCOME TO TASK MANAGER **");
    }

    private static void showError() {
        System.out.println("Unknown command! Type 'help' for supportable command!");
    }

    private static void showInfo() {
        final int processors = Runtime.getRuntime().availableProcessors();
        System.out.println("Available processors: " + processors);
        final long freeMemory = Runtime.getRuntime().freeMemory();
        System.out.println("Free memory: " + NumberUtil.formatBytes(freeMemory));
        final long maxMemory = Runtime.getRuntime().maxMemory();
        final String maxMemoryFormat = NumberUtil.formatBytes(maxMemory);
        final String maxMemoryValue = (maxMemory == Long.MAX_VALUE) ? "no limit" : maxMemoryFormat;
        System.out.println("Maximum memory: " + maxMemoryValue);
        final long totalMemory = Runtime.getRuntime().totalMemory();
        System.out.println("Total memory available to JVM: " + NumberUtil.formatBytes(totalMemory));
        final long userMemory = totalMemory - freeMemory;
        System.out.println("Used memory by JVM: " + NumberUtil.formatBytes(userMemory));
    }

    private static void parseCommand(final String command) {
        switch (command) {
            case TerminalConst.ABOUT:
                showAbout();
                break;
            case TerminalConst.VERSION:
                showVersion();
                break;
            case TerminalConst.INFO:
                showInfo();
                break;
            case TerminalConst.HELP:
                showHelp();
                break;
            case TerminalConst.COMMANDS:
                showCommands();
                break;
            case TerminalConst.ARGUMENTS:
                showArguments();
                break;
            case TerminalConst.EXIT:
                exit();
                break;
            default:
                showError();
                break;
        }
    }

    private static void parseArg(final String arg) {
        switch (arg) {
            case ArgumentConst.ABOUT:
                showAbout();
                break;
            case ArgumentConst.INFO:
                showInfo();
                break;
            case ArgumentConst.VERSION:
                showVersion();
                break;
            case ArgumentConst.HELP:
                showHelp();
                break;
            case ArgumentConst.COMMANDS:
                showCommands();
                break;
            case ArgumentConst.ARGUMENTS:
                showArguments();
                break;
            default:
                showError();
                break;
        }
    }

    private static void parseArgs(final String[] args) {
        if (args == null || args.length == 0)
            return;
        for (String param : args)
            parseArg(param);
    }

}
